import unittest
from src.main import add

class TestMain(unittest.TestCase):
    def test_add(self):
        self.assertEqual(add(1, 2), 5)
        self.assertEqual(add(-1, 2), 3)
        self.assertEqual(add(-1, -1), -3)

if __name__ == "__main__":
    unittest.main()
